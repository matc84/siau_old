package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Submissao;
import model.Usuario;

import persistence.Database;

public class SubmissaoDao {
	
	public void insert (Submissao rec) throws SQLException {
		
		Database db = Database.getInstance();
		Connection connection = db.getConnection();
				
		String sql = "insert into submissao (assunto, tipo, destino, status, matriculaDono, mensagem, data) values (?, ?, ?, ?, ?, ?, ?)";
		
		PreparedStatement preparedStmt = connection.prepareStatement(sql);
		
		preparedStmt.setString(1, rec.getAssunto());
		preparedStmt.setString(2, rec.getTipo());
		preparedStmt.setString(3, rec.getDestino());
		preparedStmt.setString(4, rec.getStatus());
		preparedStmt.setString(5, rec.getMatriculaDono());
		preparedStmt.setString(6, rec.getMensagem());
		preparedStmt.setDate(7, rec.getData());
		
		preparedStmt.execute();
		preparedStmt.close();
		
		connection.close();
		
	}

	public ArrayList<Submissao> getMessagesFrom(Usuario matricula) throws SQLException {

		Database db = Database.getInstance();
		Connection connection = db.getConnection();

		String sql = "select * from submissao where matricula='"+matricula.getMatricula()+"'";
		java.sql.Statement stmt = connection.createStatement();
		ResultSet result = stmt.executeQuery(sql);

		ArrayList<Submissao> recList = new ArrayList<Submissao>(); 

		while (result.next()) {
			Submissao rec = new Submissao(result.getString("assunto"), result.getString("tipo"), result.getString("destino"), result.getString("status"), result.getString("matriculaDono"), result.getString("mensagem"), result.getDate("data"));
			recList.add(rec);
		}

		return recList;
	}

	public ArrayList<Submissao> getAllMessages() throws SQLException {

		Database db = Database.getInstance();
		Connection connection = db.getConnection();

		String sql = "select * from submissao";
		java.sql.Statement stmt = connection.createStatement();
		ResultSet result = stmt.executeQuery(sql);

		ArrayList<Submissao> recList = new ArrayList<Submissao>(); 

		while (result.next()) {
			Submissao rec = new Submissao(result.getString("assunto"), result.getString("tipo"), result.getString("destino"), result.getString("status"), result.getString("matriculaDono"), result.getString("mensagem"), result.getDate("data"));
			recList.add(rec);
		}

		return recList;
	}

}
