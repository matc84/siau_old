package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
	
	private static Database instance = null;
	
	private Database() {
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException cnfe) {
			System.err.print("Class not found");
		}
	}
	
	public static Database getInstance() {
		if (instance == null) {
			instance =  new Database();
		}
		return instance;
	}
	
	public Connection getConnection() {
		
		try {
			return DriverManager.getConnection("jdbc:postgresql://localhost:5432/siau", "postgre", "postgre");
		} catch (SQLException e) {
			System.err.println("Could not connect");
			return null;
		}
	}
}