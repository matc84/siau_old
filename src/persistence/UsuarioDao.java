package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Usuario;

import persistence.Database;

public class UsuarioDao {
	
	public void insert (Usuario user) throws SQLException {
		
		Database db = Database.getInstance();
		Connection connection = db.getConnection();
				
		String sql = "insert into usuario (nome, matricula, email, cpf, rg, telefone, curso, senha) values (?, ?, ?, ?, ?, ?, ?, ?)";
		
		PreparedStatement preparedStmt = connection.prepareStatement(sql);
		
		preparedStmt.setString(1, user.getNome());
		preparedStmt.setString(2, user.getMatricula());
		preparedStmt.setString(3, user.getEmail());
		preparedStmt.setString(4, user.getCpf());
		preparedStmt.setString(4, user.getRg());
		preparedStmt.setString(4, user.getTelefone());
		preparedStmt.setString(4, user.getCurso());
		preparedStmt.setString(4, user.getSenha());	
		
		preparedStmt.execute();
		preparedStmt.close();
		
		connection.close();
	}
	
	public Usuario getUsuario(String matricula) throws SQLException {

		Database db = Database.getInstance();
		Connection connection = db.getConnection();

		String sql = "select * from usuario where matricula='" + matricula + "'";
		java.sql.Statement stmt = connection.createStatement();
		ResultSet result = stmt.executeQuery(sql);
		
		result.next();
		Usuario user = new Usuario(result.getString("nome"), result.getString("matricula"), result.getString("email"), result.getString("cpf"),
				result.getString("rg"), result.getString("telefone"), result.getString("curso"), result.getString("senha"));
		
		return user;
	}	
}