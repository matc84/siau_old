package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Curso;
import persistence.Database;

public class CursoDao {
	
	public void insert (Curso rec) throws SQLException {
		
		Database db = Database.getInstance();
		Connection connection = db.getConnection();
				
		String sql = "insert into cursos (codigo, curso) values (?, ?)";
		
		PreparedStatement preparedStmt = connection.prepareStatement(sql);
		
		preparedStmt.setInt(1, rec.getCodigo());
		preparedStmt.setString(2, rec.getCurso());
		
		preparedStmt.execute();
		preparedStmt.close();
		
		connection.close();
		
	}

	public Curso getCourse(Integer rec) throws SQLException {

		Database db = Database.getInstance();
		Connection connection = db.getConnection();

		String sql = "select * from cursos where id='" + rec + "'";
		java.sql.Statement stmt = connection.createStatement();
		ResultSet result = stmt.executeQuery(sql);
		
		result.next();
		Curso curso = new Curso(result.getInt("codigo"), result.getString("curso"));
		
		return curso;
	}
	
	public ArrayList<Curso> getAllCourses() throws SQLException {

		Database db = Database.getInstance();
		Connection connection = db.getConnection();

		String sql = "select * from cursos";
		java.sql.Statement stmt = connection.createStatement();
		ResultSet result = stmt.executeQuery(sql);

		ArrayList<Curso> recList = new ArrayList<Curso>(); 

		while (result.next()) {
			Curso rec = new Curso(result.getInt("codigo"), result.getString("curso"));
			recList.add(rec);
		}

		return recList;
	} 
	
}
