package model;

public class Curso {
	
	private Integer codigo;
	private String curso;
	
	public Curso(Integer codigo, String curso){
		this.codigo = codigo;
		this.curso = curso;
	}
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}
	
}
