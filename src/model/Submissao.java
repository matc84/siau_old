package model;

import java.sql.Date;

public class Submissao {
	
	private String assunto;
	private String tipo;
	private String destino;
	private String status;
	private String matriculaDono;
	private String mensagem;
	private Date data;
		
	public Submissao(String assunto, String tipo, String destino, String status, String matriculaDono, String mensagem, Date data) {
		this.assunto = assunto;
		this.tipo = tipo;
		this.destino = destino;
		this.status = status;
		this.matriculaDono = matriculaDono;
		this.mensagem = mensagem;
		this.data = data;
	}
	
	public String getAssunto() {
		return assunto;
	}
	
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getDestino() {
		return destino;
	}
	
	public void setDestino(String destino) {
		this.destino = destino;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getMatriculaDono() {
		return matriculaDono;
	}
	
	public void setMatriculaDono(String matriculaDono) {
		this.matriculaDono = matriculaDono;
	}
	
	public String getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
}