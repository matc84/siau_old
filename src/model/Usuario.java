package model;

public class Usuario {
	
	private String nome;
	private String matricula;
	private String email;
	private String cpf;
	private String rg;
	private String telefone;
	private String curso;
	private String senha;

	public Usuario(String nome, String matricula, String email, String cpf,
			String rg, String telefone, String curso, String senha) {
		this.nome = nome;
		this.matricula = matricula;
		this.email = email;
		this.cpf = cpf;
		this.rg = rg;
		this.telefone = telefone;
		this.curso = curso;
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}