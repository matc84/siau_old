package controller;

import model.Usuario;
import persistence.UsuarioDao;

import java.sql.SQLException;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;  

@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UsuarioDao dao;

    public Login() {
        super();
        dao = new UsuarioDao();
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.sendRedirect("index.jsp");
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String matricula = request.getParameter("matricula");
    	String senha = request.getParameter("senha");
    	
    	Usuario usuario;
		try {
			usuario = dao.getUsuario(matricula);
	    	if(usuario.getMatricula().equals(matricula) && (usuario.getSenha().equals(senha))){
				HttpSession sessao = request.getSession(); //obtem a sessão do usuário, caso exista
				sessao.setAttribute("matricula", matricula);
				sessao.setAttribute("usuario", usuario.getNome());
				//request.getRequestDispatcher("home.jsp").forward(request, response);
				response.sendRedirect("home.jsp");
			}
	    	else{
	    		request.setAttribute("erro", "Usuário ou senha inválidos");
	    		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
	        	dispatcher.forward(request, response);
	    	}
		} catch (SQLException e) {
			e.printStackTrace();
			request.setAttribute("erro", "Usuário ou senha inválidos");
    		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
        	dispatcher.forward(request, response);
		}
	}
}