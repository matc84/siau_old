--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cursos; Type: TABLE; Schema: public; Owner: postgre; Tablespace: 
--

CREATE TABLE cursos (
    id integer NOT NULL,
    codigo integer,
    curso character varying
);


ALTER TABLE cursos OWNER TO postgre;

--
-- Name: cursos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgre
--

CREATE SEQUENCE cursos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cursos_id_seq OWNER TO postgre;

--
-- Name: cursos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgre
--

ALTER SEQUENCE cursos_id_seq OWNED BY cursos.id;


--
-- Name: submissao; Type: TABLE; Schema: public; Owner: postgre; Tablespace: 
--

CREATE TABLE submissao (
    id integer NOT NULL,
    assunto character varying,
    tipo character varying,
    destino character varying,
    status character varying,
    "matriculaDono" character varying,
    mensagem text,
    data timestamp without time zone
);


ALTER TABLE submissao OWNER TO postgre;

--
-- Name: submissao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgre
--

CREATE SEQUENCE submissao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE submissao_id_seq OWNER TO postgre;

--
-- Name: submissao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgre
--

ALTER SEQUENCE submissao_id_seq OWNED BY submissao.id;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgre; Tablespace: 
--

CREATE TABLE usuario (
    id integer NOT NULL,
    nome character varying,
    matricula character varying,
    email character varying,
    cpf character varying,
    rg character varying,
    telefone character varying,
    curso character varying,
    senha character varying
);


ALTER TABLE usuario OWNER TO postgre;

--
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgre
--

CREATE SEQUENCE usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario_id_seq OWNER TO postgre;

--
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgre
--

ALTER SEQUENCE usuario_id_seq OWNED BY usuario.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgre
--

ALTER TABLE ONLY cursos ALTER COLUMN id SET DEFAULT nextval('cursos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgre
--

ALTER TABLE ONLY submissao ALTER COLUMN id SET DEFAULT nextval('submissao_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgre
--

ALTER TABLE ONLY usuario ALTER COLUMN id SET DEFAULT nextval('usuario_id_seq'::regclass);


--
-- Name: cursos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgre; Tablespace: 
--

ALTER TABLE ONLY cursos
    ADD CONSTRAINT cursos_pkey PRIMARY KEY (id);


--
-- Name: submissao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgre; Tablespace: 
--

ALTER TABLE ONLY submissao
    ADD CONSTRAINT submissao_pkey PRIMARY KEY (id);


--
-- Name: usuario_cpf_key; Type: CONSTRAINT; Schema: public; Owner: postgre; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_cpf_key UNIQUE (cpf);


--
-- Name: usuario_matricula_key; Type: CONSTRAINT; Schema: public; Owner: postgre; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_matricula_key UNIQUE (matricula);


--
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgre; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- Name: usuario_rg_key; Type: CONSTRAINT; Schema: public; Owner: postgre; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_rg_key UNIQUE (rg);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

