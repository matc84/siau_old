<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
  if(null == session.getAttribute("matricula")){
    response.sendRedirect("index.jsp");
  }
%>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>SiAu - UFBA</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
    <link rel="shortcut icon" href="static/img/ufba.png" />
    <!-- Bootstrap 3.3.2 -->
    <link href="static/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="static/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="static/css/ionicons.min.css" rel="stylesheet" type="text/css" /> 
    <!-- Theme style -->
    <link href="static/css/style.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue">
    <div class="wrapper">
      <header class="main-header">
        <a href="/siau/home.html" class="logo"><b>SiAu </b>UFBA</a>
        <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="glyphicon glyphicon-user"></i><span><%=session.getAttribute("usuario")%> <i class="caret"></i></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="user-header">
                    <img src="static/img/brasao-ufba.jpg" class="img-circle" alt="User Image" />
                    <p>
                      <%=session.getAttribute("usuario")%>
                    </p>
                  </li>
                  <li class="user-footer">
                    <div class="pull-right">
                      <a href="/siau/logout" class="btn btn-default btn-flat">Sair</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <aside class="main-sidebar">
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="static/img/brasao-ufba.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><%=session.getAttribute("matricula")%></p>
              <i class="fa fa-circle text-success"></i> Online
            </div>
          </div>
          <ul class="sidebar-menu">
            <li>
              <a href="/siau/home.jsp">
                <i class="fa fa-plus"></i> <span>Submeter reclamações</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-bar-chart-o"></i> <span>Status do Ticket</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-envelope-o"></i> <span>Contato</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-bullhorn"></i> <span>Sobre</span>
              </a>
            </li>
          </ul>
        </section>
      </aside>

      <div class="content-wrapper">
        <section class="content-header">
          <h1>Submeter reclamações</h1>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-lg-12">
              <div class="box box-primary">
                <form method="POST" id="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label>Assunto:</label>
                      <input type="text" id="assunto" class="form-control" required>
                    </div>
                    <div class="form-group">
                      <label>Tipo da mensagem:</label>
                      <select id="tipo" class="form-control" required>
                        <option value="">Selecione...</option>
                        <option value="Reclamação">Reclamação</option>
                        <option value="Sugestão">Sugestão</option>
                        <option value="Dúvida">Dúvida</option>
                        <option value="Outro">Outro</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Destino da mensagem:</label>
                      <select id="destino" class="form-control" required>
                        <option value="">Selecione...</option>
                        <option value="Colegiado de Ciência da Computação">Colegiado de Ciência da Computação</option>
                        <option value="Colegiado de Sistemas de Informação">Colegiado de Sistemas de Informação</option>
                        <option value="Colegiado de Licenciatura em Computação">Colegiado de Licenciatura em Computação</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Mensagem:</label>
                      <textarea rows="5" id="mensagem" class="form-control" required></textarea>
                    </div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submeter</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </body>
  <!-- jQuery 2.1.3 -->
  <script src="static/js/jQuery-2.1.3.min.js" type="text/javascript"></script>
  <!-- Bootstrap 3.3.2 JS -->
  <script src="static/js/bootstrap.js" type="text/javascript"></script>
  <!-- AdminLTE App -->
  <script src="static/js/app.js" type="text/javascript"></script>
  <!--  Ativar elementos do menu dinamicamente -->
  <script type="text/javascript">
    $(function () {
        setNavigation();
    });

    function setNavigation() {
        var path = window.location.pathname;
        path = path.replace(/\/$/, "");
        path = decodeURIComponent(path);

        $(".sidebar-menu a").each(function () {
          var href = $(this).attr('href');
            if (path.substring(0, href.length) === href) {
              $(this).closest('li.treeview').addClass('active');
              $(this).closest('li').addClass('active');
            }
        });
    }
  </script>
</html>