<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="persistence.CursoDao" %>
<%
    CursoDao c = new CursoDao();
%>
<%
  if(null != session.getAttribute("matricula")){
    response.sendRedirect("home.jsp");
  }
%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>SiAu - UFBA</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
    <link rel="shortcut icon" href="static/img/ufba.png" />
    <!-- Bootstrap 3.3.2 -->
    <link href="static/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="static/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="static/css/style.css" rel="stylesheet" type="text/css" />
    <!-- Evita retornar com o botão de voltar do navegador após o logout -->
    <script type="text/javascript" >
      history.pushState(null, null, '');
      window.addEventListener('popstate', function(event) {
        history.pushState(null, null, '');
      });
    </script>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <img src="static/img/logo.png"><br><br>
      </div>
      <div class="login-box-body">
        <p class="login-box-msg" style="font-size:17px;font-weight:bold">Faça login para iniciar a sua sessão</p>
        <form action="/siau/login" method="post">
          <div class="form-group has-feedback">
            <input type="text" name="matricula" class="form-control" placeholder="Matrícula" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="senha" class="form-control" placeholder="Senha" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
              <br><a href="#" onclick="exibir_modal()">Criar uma conta</a>
            </div>
          </div>
          <% if (request.getAttribute("erro") != null){ %>
          <br><div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            <% out.println(request.getAttribute("erro")); %>
          </div>
          <% } %>
        </form>
      </div>
    </div>
    <!-- MODAL CADASTRO -->
    <div class="modal fade" id="cadastro" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class="fa fa-plus"></i> Cadastro de usuário</h4>
          </div>
          <div class="modal-body">
            <form action="#" method="post">
              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="nome" placeholder="Nome completo" required />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="email" placeholder="E-mail" required />
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="matricula" placeholder="Matrícula" required />
                <span class="glyphicon fa-credit-card form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" name="senha" placeholder="Senha" required />
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
              	<select name="curso" class="form-control" required>
              		<option value="">Selecione seu curso...</option>
              		<!-- <% /* for(int i=1; i <= 118; i++){ */ %>
        			  <option value="<% /* out.print(i); */ %> "><% /* out.print(c.getCourse(i).getCurso()); */ %></option>
    				<% /* } */%> -->
              	</select>
              </div>
              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="cpf" placeholder="CPF" required />
                <span class="glyphicon glyphicon glyphicon-ok-circle form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="rg" placeholder="RG" required />
                <span class="glyphicon gglyphicon glyphicon-ok-sign form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="telefone" placeholder="Telefone" data-inputmask='"mask": "(99) 9999-9999"' data-mask required />
                <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Cadastrar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>
  <script src="static/js/jQuery-2.1.3.min.js"></script>
  <script src="static/js/bootstrap.js" type="text/javascript"></script>
  <script src="static/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
  <script type="text/javascript">
      $(function () {
        $("[data-mask]").inputmask();
      });
  </script>
  <script type="text/javascript">
    function exibir_modal(){
      $("#cadastro").modal("show");
    }
  </script>
</html>